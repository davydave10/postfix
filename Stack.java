import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Stack<X>
{
    private List<X> ll;
    
    public Stack()
    {
        ll = new LinkedList<X>();
    }
    
    public void push(X item)
    {
        ll.add(item);
    }
    
    public X pop()
    {
        try
        {
            X x = ll.get(ll.size()-1);
            ll.remove(ll.size()-1);
            return x;
        }
        catch (java.lang.IndexOutOfBoundsException ioobe)
        {
            throw new StackUnderflowException();
        }
    }
    
    public X peek()
    {
        return ll.get(ll.size()-1);
    }
    
    public boolean isEmpty()
    {
        return ll.size() == 0;
    }
    
    public void clear()
    {
        ll.clear();
    }
}