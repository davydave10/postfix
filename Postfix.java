//ask about Double.parseDouble(numberAsString)
public class Postfix
{
    public double eval(String expr)
    {
        String[] tokens = expr.split(" ");
        Stack<Double> s = new Stack<Double>();
        
        for (int i = 0; i < tokens.length; i++)
        {
            String token = tokens[i];
            
            switch(token)
            {
                    case "/":
                    {
                       try{
                        double a = s.pop();
                        double b = s.pop();
                        s.push(b / a);
                       }
                       catch(StackUnderflowException e){
                        String cause = e.getMessage();
                        System.err.println("StackUnderflowException: "+cause);
                       }
                    break;
                    }
                    
                
                    case "+":
                    {
                       try{
                        double a = s.pop();
                        double b = s.pop();
                        s.push(b + a);
                       }
                       catch(StackUnderflowException e){
                        String cause = e.getMessage();
                        System.err.println("StackUnderflowException: "+cause);
                       }
                    break;
                    }
                    
                    case "*":
                    {
                         try{
                            double a = s.pop();
                            double b = s.pop();
                            s.push(b * a);
                          }
                       catch(StackUnderflowException e){
                        String cause = e.getMessage();
                        System.err.println("StackUnderflowException: "+cause);
                           }
                        break;
                    }
                    
                    case "-":
                    {
                      try{
                            double a = s.pop();
                            double b = s.pop();
                            s.push(b - a);
                        }
                        catch(StackUnderflowException e){
                            String cause = e.getMessage();
                            System.err.println("StackUnderflowException: "+cause);
                        }
                        break;
                    }
                    
                    case "^":
                    {
                        try{
                            double a = s.pop();
                            double b = s.pop();
                            s.push(Math.pow(b, a));//Hmmmmm
                        }
                        catch(StackUnderflowException e){
                            String cause = e.getMessage();
                            System.err.println("StackUnderflowException: "+cause);
                        }
                        break;
                    }
                    
                    case "sin":
                    {
                        try{
                            double a = s.pop();
                            s.push(Math.sin(Math.toRadians(a)));
                        }
                        catch(StackUnderflowException e){
                            String cause = e.getMessage();
                            System.err.println("StackUnderflowException: "+cause);
                        }
                        break;
                    }
                    
                    case "cos":
                    {
                        try{
                            double a = s.pop();
                            s.push(Math.sin(Math.toRadians(a)));
                        }
                         catch(StackUnderflowException e){
                            String cause = e.getMessage();
                            System.err.println("StackUnderflowException: "+cause);
                        }
                        break;
                    }
              
                    case "tan":
                    {
                       try{
                            double a = s.pop();
                            s.push(Math.sin(Math.toRadians(a)));
                       }
                       catch(StackUnderflowException e){
                            String cause = e.getMessage();
                            System.err.println("StackUnderflowException: "+cause);
                        }
                        break;
                    }
                    
                    case "pi":
                    {
                        s.push(Math.PI);
                        break;
                    }
                    
                    default:
                       try{
                           s.push(Double.parseDouble(token));
                       }
                       catch(InvalidTokenException e){
                            String cause = e.getMessage();
                            System.err.println("InvalidTokenException: "+cause);
                        }
                    break;
                
            }

        }
        
        return s.peek();
    }
  }

