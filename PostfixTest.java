

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class PostfixTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class PostfixTest
{
    /**
     * Default constructor for test class PostfixTest
     */
    public PostfixTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testArthimetic()
    {
        Postfix p = new Postfix();
        assertEquals(13, p.eval("8 5 +"), 0.001);
        assertEquals(3 , p.eval("8 5 -"), 0.001);
        assertEquals(40 , p.eval("8 5 *"), 0.001);
        assertEquals(1024 , p.eval("2 10 ^"), 0.001);
    }
    
    @Test
    public void testCombined()
    {
        Postfix p = new Postfix();
        assertEquals(0 , p.eval("8 5 + 13 -"), 0.001);
        assertEquals(-10  , p.eval("8 5 13 + -"), 0.001);
        assertEquals(256,  p.eval("2 2 3 ^ ^"), 0.001);
    }
    
    @Test (expected=InvalidTokenException.class)
    public void testNotANumber()
    {
        Postfix p = new Postfix();
        p.eval("e ^ 2");
    }
    
    @Test (expected=InvalidTokenException.class)
    public void testNotAnOperator()
    {
        Postfix p = new Postfix();
        p.eval("3 % 2");
    }
    
    @Test (expected=StackUnderflowException.class)
    public void testStackUnderflow()
    {
        Postfix p = new Postfix();
        p.eval("+");
    }
    
    @Test
    public void testTrig()
    {
        Postfix p = new Postfix();
        assertEquals( 1 , p.eval("pi 2 / sin"), 0.001);
        assertEquals( 0 , p.eval("pi 2 / cos"), 0.001);
        assertEquals( 1 , p.eval("pi 4 / tan"), 0.001);
    }
}

